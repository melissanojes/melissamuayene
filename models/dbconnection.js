const mysql = require('mysql');
  
var connection;



module.exports = {

    melissaConnection: function (sql,callback) {

        connection = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: '',
            database: 'melissa',
            debug: false,
            multipleStatements: true
        });
        connection.connect();

        connection.query(sql, function(error, results, fields) {   
            callback(results);
        });
        connection.end();

    },
    skrsConnection: function (sql,callback) {

        connection = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: '',
            database: 'skrs',
            debug: false,
            multipleStatements: true,
            
        });
        connection.connect();

        connection.query(sql, function(error, results, fields) {   
            callback(results);
        });
        connection.end();

    }


};