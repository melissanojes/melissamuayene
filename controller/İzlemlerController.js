var hastaModel = require('../models/HastaModel');
var db = require('../models/dbconnection');

var izlemModel = require('../models/IzlemModel');
var izlemKaydetModel = require('../models/IzlemKaydetModel');





module.exports.index=function(req,res){

   

    if(req.query.izlemAdı=="kadinizlem"){
		hastaModel.getirKadınHastalar(function(data){
			res.render('izlemler',{ 
				izlemAdı:req.query.izlemAdı,
				izleminHastaları:data
			});
		});
    }
    if(req.query.izlemAdı=="bebekcocukizlem"){
        hastaModel.getirCocukHastalar(function(data){
			res.render('izlemler',{ 
				izlemAdı:req.query.izlemAdı,
				izleminHastaları:data
			});
        });
    }
    if(req.query.izlemAdı=="gebeizlem"){
        hastaModel.getirGebeHastalar(function(data){
			res.render('izlemler',{ 
				izlemAdı:req.query.izlemAdı,
				izleminHastaları:data
			});
        });
    }
    if(req.query.izlemAdı=="lohusaizlem"){
        hastaModel.getirLohusaHastalar(function(data){
			res.render('izlemler',{ 
				izlemAdı:req.query.izlemAdı,
				izleminHastaları:data
			});
        });
    }
    if(req.query.izlemAdı=="kanserHedefListesi"){
        var izleminHastaları=[{isimSoyisim:"Aras Öner",TC:"12312313",Yas:"23"},{isimSoyisim:"Emre EREL",TC:"12312313",Yas:"23"},{isimSoyisim:"Asena asena",TC:"12312313",Yas:"23"}];
    }

    if(req.query.izlemAdı=="yetiskinizlem"){

            hastaModel.getirYetişkinHastalar(function(data){
                res.render('izlemler',{ 
                    izlemAdı:req.query.izlemAdı,
                    izleminHastaları:data
                });
            });
    }
    if(req.query.izlemAdı=="obeziteizlem"){
        hastaModel.getirObeziteHastalar(function(data){
			res.render('izlemler',{ 
				izlemAdı:req.query.izlemAdı,
				izleminHastaları:data
			});
		});
    }
    if(req.query.izlemAdı=="otizmizlem"){
        hastaModel.getirOtizmHastalar(function(data){
            res.render('izlemler',{ 
                izlemAdı:req.query.izlemAdı,
                izleminHastaları:data
            });
        });
    }
}
function getAge(DogumTarihi){

    var birthdate =new Date(DogumTarihi);
		var now = new Date();
		var dayDif=now.getDate()-birthdate.getDate();
		var yearDif=now.getFullYear()-birthdate.getFullYear();
		var monthDif=now.getMonth()-birthdate.getMonth();

		if(dayDif<0){monthDif--;dayDif+=30;if(monthDif<0){yearDif--;monthDif+=12;}}
		else{if(monthDif<0){yearDif--;monthDif+=12;}}
		var age="";
		if(yearDif>0){age+=yearDif+" Yıl ";}
		if(monthDif>0){age+=monthDif+" Ay ";}
		if(dayDif>0){age+=dayDif+" Gün ";}
        return age;

}
module.exports.kadınİzlem=function(req,res){
    var hastaTc=req.body.hasta;

    izlemModel.kadınIzlem(hastaTc,function(MelissaResults,SkrsResults){
            var hasta=MelissaResults[0][0];
            if(MelissaResults[1][0]==undefined){sonAşı={Tarih:new Date()}}
            else{sonAşı=MelissaResults[1][0];sonAşı}
            if(SkrsResults[8][0]==undefined){skrsSonAşı={ADI:"null"}}
            else{skrsSonAşı=SkrsResults[8][0];}
            hasta.age=getAge(hasta.DogumTarihi);
            var params={
                hasta:hasta,
                APKullanmamaNedeni:SkrsResults[0],
                APYöntemleri:SkrsResults[1],
                KadinSaglikIslemleri:SkrsResults[2],
                KadinSaglikRiskFaktorleri:SkrsResults[3],
                APYontemLojistik:SkrsResults[4],
                APOncekiYontem:SkrsResults[5],
                SiddetTuru:SkrsResults[6],
                SiddetSonucuYonlendirme:SkrsResults[7],
                asilar:SkrsResults[9],
                asiuygulamasekli:SkrsResults[10],
                asikaynak:SkrsResults[11],
                asiuygulamayeri:SkrsResults[12],
                asidoz:SkrsResults[13],
                sonAşı:sonAşı,
                skrsSonAşı:skrsSonAşı,

            };	
            res.render('kadinizlem',params);      
         
    });
            

}
module.exports.kadınizlemKaydet=function(req,res){

    const form = JSON.parse(JSON.stringify(req.body))
 
     izlemKaydetModel.kadınKaydet(form,function(MelissaResults){          
    });
    res.redirect("/");

}

module.exports.bebekcocukizlem=function(req,res){
    var hastaTc=req.body.hasta;
    izlemModel.bebekCocukIzlem(hastaTc,function(MelissaResults,SkrsResults){
            MelissaResults[0].age=getAge(MelissaResults[0].DogumTarihi);
            req.session.hasta=MelissaResults[0];
           
            var params={
                hasta:MelissaResults[0],
                DogumYontemi:SkrsResults[0],
                DogumAgirligi:SkrsResults[1],
            };	
      
            res.render('bebekCocukizlem',params);       
    });          
}
module.exports.bebekcocukYeniIzlem=function(req,res){
    var hastaTc=req.session.hasta.Id;
    izlemModel.bebekcocukYeniIzlem(hastaTc,function(MelissaResults,SkrsResults){
        MelissaResults[0].age=getAge(MelissaResults[0].DogumTarihi);

            var islemGrubu=[{ADI:"Beyin Gelişimi için Riskler",KODU:"BeyinGelisimiRiskler"},
                            {ADI:"Ebeveynin Yaptığı Aktiviteler",KODU:"EbeveynAktivite"},
                            {ADI:"Gelişim Tablosu Bilgisinin Sorgulanması",KODU:"GelisimTablosuBilgilerininSorgulanmasi"},
                            {ADI:"Risklere Yönelik Eğitimler",KODU:"RisklereYonelikEgtimler"},
                            {ADI:"Risk Faktörlerine Yapılan Müdahaleler",KODU:"RiskFaktorlerineYapilanMudaheleler"},
                            {ADI:"Risk Altindaki Olgunun Takibi",KODU:"RiskTakipi"},

                            ];
            var params={
                hasta:MelissaResults[0],
                islemGrubu:islemGrubu,
                DVitamini:SkrsResults[0],
                DemirDestek:SkrsResults[1],
                BeyinGelisimiRiskler:SkrsResults[2],
                kacinciizlem:SkrsResults[3],
                BeyinGelisimiRiskler:SkrsResults[4],
                EbeveynAktivite:SkrsResults[5],
                GelisimTablosuBilgilerininSorgulanmasi:SkrsResults[6],
                RisklereYonelikEgtimler:SkrsResults[7],
                RiskFaktorlerineYapilanMudaheleler:SkrsResults[8],
                RiskTakipi:SkrsResults[9],

            };	   
            res.render('bebekcocukizlem-YeniIzlem',params);               
    });                                    
}
module.exports.bebekcocukKaydet=function(req,res){
    const form = JSON.parse(JSON.stringify(req.body))
    if(form.konanomali==undefined){
        form.konanomali=0;
    }
    if(form.serviksmear==undefined){
        form.serviksmear=0;
    }
    if(form.talesemi==undefined){
        form.talesemi=0;
    }
    if(form.hbs==undefined){
        form.hbs=0;
    }
    console.log(JSON.stringify(form))

    //  izlemKaydetModel.kadınKaydet(form,function(MelissaResults){          
    // });
    // res.redirect("/");
}


module.exports.gebeizlem=function(req,res){
    var hastaTc=req.body.hasta;
    izlemModel.gebeIzlem(hastaTc,function(MelissaResults,SkrsResults){
        MelissaResults[0].age=getAge(MelissaResults[0].DogumTarihi);

        req.session.hasta=MelissaResults[0];
        
            var params={
                hasta:MelissaResults[0],
                DogumYontemi:SkrsResults[0],
                DogumAgirligi:SkrsResults[1],
            };	
      
      
        res.render('gebeizlem',params);               
    });          
}
module.exports.gebeYeniIzlem=function(req,res){
    var hastaTc=req.session.hasta.Id;
    izlemModel.gebeYeniIzlem(hastaTc,function(MelissaResults,SkrsResults){
            MelissaResults[0].age=getAge(MelissaResults[0].DogumTarihi);
            var islemGrubu=[{ADI:"Gebelikte Risk Faktörleri",KODU:"GebeRiskFaktor"},{ADI:"Gebelikte Tehlike İşaretleri",KODU:"GebeTehlikeIsaret"}];
            var params={
                islemGrubu:islemGrubu,
                hasta:MelissaResults[0],
                GelisSekli:SkrsResults[0],
                KonjenitalAnomali:SkrsResults[1],
                DVitamini:SkrsResults[2],
                DemirDestek:SkrsResults[3],
                GebeRiskFaktor:SkrsResults[4],
                GebeTehlikeIsaret:SkrsResults[5],
                idrardaProtein:SkrsResults[6],
            };	
            res.render('gebeizlem-YeniIzlem',params);      
    });          
}
module.exports.gebeizlemKaydet=function(req,res){

    const form = JSON.parse(JSON.stringify(req.body))

     izlemKaydetModel.gebeKaydet(form,function(MelissaResults){          
    });
    res.redirect("/");

}


module.exports.diğerGebelikler=function(req,res){
    var hastaTc=req.session.hasta.Id;
    izlemModel.diğerGebelikler(hastaTc,function(MelissaResults,SkrsResults){
        MelissaResults[0].age=getAge(MelissaResults[0].DogumTarihi);

            var params={
                hasta:MelissaResults[0],
                GelisSekli:SkrsResults[0],
                KonjenitalAnomali:SkrsResults[1],
                DVitamini:SkrsResults[2],
                DemirDestek:SkrsResults[3],
            };	
            res.render('gebeizlem-digerGebelikler',params);               
    });                   
}
module.exports.gebelikSonlandır=function(req,res){
    var hastaTc=req.session.hasta.Id;
    izlemModel.gebelikSonlandır(hastaTc,function(MelissaResults,SkrsResults){
        MelissaResults[0].age=getAge(MelissaResults[0].DogumTarihi);
  
            var gebelikler=[{TC:"12312412412",ad:"Gizem Soyadıbilinmez",cinsiyet:"Kadın"},{TC:"54623523523",ad:"Aras Öner",cinsiyet:"Erkek"}];
            var params={
                hasta:MelissaResults[0],
                gebelikler:gebelikler,
                GebelikSonucu:SkrsResults[0],
                DogumYontemi:SkrsResults[1],
                DogumaYardimEden:SkrsResults[2],
                DogumGerceklestigiYer:SkrsResults[3],
            };	
            res.render('gebeizlem-gebelikSonlandır',params);               
    });          
}

module.exports.lohusaizlem=function(req,res){
    var hastaTc=req.body.hasta;
    izlemModel.lohusaIzlem(hastaTc,function(MelissaResults,SkrsResults){
        MelissaResults[0].age=getAge(MelissaResults[0].DogumTarihi);
        req.session.hasta=MelissaResults[0];          

            var params={
                hasta:MelissaResults[0],
                DogumYontemi:SkrsResults[0],
                DogumAgirligi:SkrsResults[1],
                DVitamini:SkrsResults[2],
                DemirDestek:SkrsResults[3],
            };	
            res.render('lohusaizlem',params);               
    });          
}
module.exports.lohusaYeniIzlem=function(req,res){
    var hastaTc=req.session.hasta.Id;
    izlemModel.lohusaYeniIzlem(hastaTc,function(MelissaResults,SkrsResults){
        MelissaResults[0].age=getAge(MelissaResults[0].DogumTarihi);

            var params={
                hasta:MelissaResults[0],
                DVitamini:SkrsResults[0],
                DemirDestek:SkrsResults[1],
                UterusInvolusyon:SkrsResults[2],
                PostpartumDepresyon:SkrsResults[3],
                KonjenitalAnomali:SkrsResults[4],
                GebeTehlikeIsaret:SkrsResults[5],
                KadinSaglikIslemleri:SkrsResults[6],
                APYöntemleri:SkrsResults[7],
                kacinciLohusa:SkrsResults[8],

            };	
            res.render('lohusaizlem-YeniIzlem',params);               
    });                         
}
module.exports.lohusaizlemKaydet=function(req,res){

    const form = JSON.parse(JSON.stringify(req.body))
console.log(JSON.stringify(form))
     izlemKaydetModel.lohusaKaydet(form,function(MelissaResults){          
    });
    res.redirect("/");

}

module.exports.yetiskinizlem=function(req,res){
    var hastaTc=req.body.hasta;

    izlemModel.yetiskinIzlem(hastaTc,function(MelissaResults,SkrsResults){
        MelissaResults[0].age=getAge(MelissaResults[0].DogumTarihi);
        nowDate=new Date()
        var date1 =new Date(MelissaResults[0].DogumTarihi);
        var date2 = new Date(); 

        // To calculate the time difference of two dates 
        var Difference_In_Time = date2.getTime() - date1.getTime(); 
  
        // To calculate the no. of days between two dates 
        var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24); 
        Difference_In_Days = Math.round(Difference_In_Days);

            var params={
                hasta:MelissaResults[0],
                OkulCagiPostur:SkrsResults[0],
                GormeTaramaSonucu:SkrsResults[1],
                ergenIzlemTakvimi:SkrsResults[2],
                totalDay:Difference_In_Days
            }	
            res.render('yetiskinizlem',params);               
    });
}
module.exports.yetiskinizlemKaydet=function(req,res){

    const form = JSON.parse(JSON.stringify(req.body))

    if(form.sdd==undefined){
        form.sdd=0;
    }
    if(form.heeadsss==undefined){
        form.heeadsss=0;
    }
    if(form.gelisimseld==undefined){
        form.gelisimseld=0;
    }
    if(form.hiperd==undefined){
        form.hiperd=0;
    }
    if(form.dissagligi==undefined){
        form.dissagligi=0;
    }
    if(form.isitmet==undefined){
        form.isitmet=0;
    }
    if(form.gormet==undefined){
        form.gormet=0;
    }
    if(form.bagisildama==undefined){
        form.bagisildama=0;
    }
    if(form.danismanlik==undefined){
        form.danismanlik=0;
    }
    if(form.fizikmyn==undefined){
        form.fizikmyn=0;
    }



    izlemKaydetModel.yetiskinKaydet(form,function(MelissaResults,SkrsResults){          
    });
    res.redirect("/");

}
module.exports.obeziteizlem=function(req,res){
    var hastaTc=req.body.hasta;

    izlemModel.obeziteIzlem(hastaTc,function(MelissaResults,SkrsResults){
        MelissaResults[0].age=getAge(MelissaResults[0].DogumTarihi);
            var params={
                hasta:MelissaResults[0],
                MorbidObez:SkrsResults[0],
                Egzersiz:SkrsResults[1],
                DiyetTedavisi:SkrsResults[2],
                ObeziteIlacTedavisi:SkrsResults[3],
                PsikolojikTedavi:SkrsResults[4],
                YatagaBagimlilik:SkrsResults[5],
                EkTanilar:SkrsResults[6]
            }
            res.render('obeziteizlem',params);               
    });
}
module.exports.obeziteizlemKaydet=function(req,res){
    const form = JSON.parse(JSON.stringify(req.body))
    if(form.adimsayar==undefined){
        form.adimsayar=0;
    }
    if(form.geziciHizmet==undefined){
        form.geziciHizmet=0;
    }    

    izlemKaydetModel.obeziteKaydet(form,function(MelissaResults){});
    res.redirect("/");
}
module.exports.otizmizlem=function(req,res){
    var hastaTc=req.body.hasta;

    izlemModel.otizmIzlem(hastaTc,function(MelissaResults,SkrsResults){
        MelissaResults[0].age=getAge(MelissaResults[0].DogumTarihi);
      
            var params={
                hasta:MelissaResults[0],
                MorbidObez:SkrsResults[0],
                Egzersiz:SkrsResults[1],
            };	
            res.render('otizmizlem',params);      
    });
}
module.exports.otizmKaydet=function(req,res){

    const form = JSON.parse(JSON.stringify(req.body))
   console.log(form);
 
     izlemKaydetModel.otizmKaydet(form,function(MelissaResults){          
    });
    res.redirect("/");

}
module.exports.kanserizlem=function(req,res){

        res.render('kanserizlem');    
}





module.exports.evdeSaglıkizlem=function(req,res){

   res.render("evdesaglık");
        

}
module.exports.ESbasvuruListesi=function(req,res){
    
        res.render('ES-BasvuruListesi');    

}
module.exports.EShizmetSonlandırmaListesi=function(req,res){

         res.render('ES-HizmetSonlandırmaListesi');    
    
}

module.exports.ESHizmetiIzlemleri=function(req,res){

    var hastaTc=12312312312;

    izlemModel.ESHizmetiIzlemleri(hastaTc,function(MelissaResults,SkrsResults){
        MelissaResults[0].age=getAge(MelissaResults[0].DogumTarihi);

            var params={
                hasta:MelissaResults[0],
                iller:SkrsResults[0],
                ESSonlandirma:SkrsResults[1],
            };	
            res.render('ES-Hizmetİzlemleri',params);               
    });



    
}
module.exports.ESHastaNakilListesi=function(req,res){

    
         res.render('ES-HastaNakilListesi');    
    
}
module.exports.ESİlkİzlem=function(req,res){
    var hastaTc=12312312312;

    izlemModel.ESİlkİzlem(hastaTc,function(MelissaResults,SkrsResults){
        MelissaResults[0].age=getAge(MelissaResults[0].DogumTarihi);

      
            var params={
                hasta:MelissaResults[0],
                BasvuruTuru:SkrsResults[0],
                BakimDestekihtiyaci:SkrsResults[1],
                KisiselBakim:SkrsResults[2],
                KonutTipi:SkrsResults[3],
                HelaTipi:SkrsResults[4],
            };	
            res.render('ES-İlkİzlem',params);               
    });

    
}


module.exports.kanserHedeflistesi=function(req,res){

    hastaModel.getirYetişkinHastalar(function(data){
                console.log(data)
        res.render('kanserHedefListesi',{ 
            izleminHastaları:data
        });
    });
}

module.exports.kanserHedeflistesiHasta=function(req,res){

    var hastaTc=req.body.hasta;
    izlemModel.kanserHedeflistesiHasta(hastaTc,function(MelissaResults,SkrsResults){
        MelissaResults[0].age=getAge(MelissaResults[0].DogumTarihi);
        req.session.hasta=MelissaResults[0];
       
            var params={
                hasta:MelissaResults[0],
                HPVTipleri:SkrsResults[0],
                ServikalSitoloji:SkrsResults[1]
            }	
            res.render('kanserHedefListesi-Hasta',params);      
    });
}

module.exports.kanserHedeflistesiYeniTarama=function(req,res){
    var hastaTc=req.session.hasta.tckimlikno;
    izlemModel.kanserHedeflistesiYeniTarama(hastaTc,function(MelissaResults,SkrsResults){
        MelissaResults[0].age=getAge(MelissaResults[0].DogumTarihi);
             
            var params={
                hasta:MelissaResults[0],
                TaramaTürü:SkrsResults[0],
                GaitadaGizliKanTesti:SkrsResults[1],
                HPVTipleri:SkrsResults[2],
                ServikalSitoloji:results[3],
                KendiKendineMemeMuayenesi:SkrsResults[4],
                KlinikMemeMuayenesi:SkrsResults[5],
                PapSmearTesti:SkrsResults[6],
                HPVTaramaTesti:results[7],
                KolonGörüntülemeYöntemi:SkrsResults[8],
                Mamografi:SkrsResults[9],
                MamografiSonucu:SkrsResults[10],
            };	
            res.render('kanserHedefListesi-YeniTarama',params);               
    });          
}




module.exports.kanserTaramaAnalizEkranı=function(req,res){
    var sql = "SELECT * FROM TaramaTürü WHERE AKTIF = 'true';SELECT * FROM GaitadaGizliKanTesti WHERE AKTIF = 'true';SELECT * FROM HPVTipleri WHERE AKTIF = 1;SELECT * FROM ServikalSitoloji WHERE AKTIF = 1;SELECT * FROM KendiKendineMemeMuayenesi WHERE AKTIF = 'true';";
    sql+="SELECT * FROM KlinikMemeMuayenesi WHERE AKTIF = 'true';SELECT * FROM PapSmearTesti WHERE AKTIF = 'true';SELECT * FROM HPVTaramaTesti WHERE AKTIF = 'true';SELECT * FROM KolonGörüntülemeYöntemi WHERE AKTIF = 'true';";
    sql+="SELECT * FROM Mamografi WHERE AKTIF = 'true';SELECT * FROM MamografiSonucu WHERE AKTIF = 'true';";

    skrs.skrsConnection(sql,function(results){

        var params={
            TaramaTürü:results[0],
            GaitadaGizliKanTesti:results[1],
            HPVTipleri:results[2],
            ServikalSitoloji:results[3],
            KendiKendineMemeMuayenesi:results[4],
            KlinikMemeMuayenesi:results[5],
            PapSmearTesti:results[6],
            HPVTaramaTesti:results[7],
            KolonGörüntülemeYöntemi:results[8],
            Mamografi:results[9],
            MamografiSonucu:results[10],

        };	
  
  
        res.render('kanserTaramaAnalizEkranı',params);      
    });
     
}
module.exports.topluHPVSonucSorgulama=function(req,res){
    var sql = "SELECT * FROM TaramaTürü WHERE AKTIF = 'true';SELECT * FROM GaitadaGizliKanTesti WHERE AKTIF = 'true';SELECT * FROM HPVTipleri WHERE AKTIF = 1;SELECT * FROM ServikalSitoloji WHERE AKTIF = 1;SELECT * FROM KendiKendineMemeMuayenesi WHERE AKTIF = 'true';";
    sql+="SELECT * FROM KlinikMemeMuayenesi WHERE AKTIF = 'true';SELECT * FROM PapSmearTesti WHERE AKTIF = 'true';SELECT * FROM HPVTaramaTesti WHERE AKTIF = 'true';SELECT * FROM KolonGörüntülemeYöntemi WHERE AKTIF = 'true';";
    sql+="SELECT * FROM Mamografi WHERE AKTIF = 'true';SELECT * FROM MamografiSonucu WHERE AKTIF = 'true';SELECT * FROM MedeniHali WHERE AKTIF = 1;";

    skrs.skrsConnection(sql,function(results){

        var params={
            TaramaTürü:results[0],
            GaitadaGizliKanTesti:results[1],
            HPVTipleri:results[2],
            ServikalSitoloji:results[3],
            KendiKendineMemeMuayenesi:results[4],
            KlinikMemeMuayenesi:results[5],
            PapSmearTesti:results[6],
            HPVTaramaTesti:results[7],
            KolonGörüntülemeYöntemi:results[8],
            Mamografi:results[9],
            MamografiSonucu:results[10],
            MedeniHali:results[11],
        };	
  
  
        res.render('topluHPVSonucSorgulama',params);      
    });
}
