var hastaModel = require('../models/HastaModel');
var db = require('../models/dbconnection');
var hastaProfilModel=require('../models/HastaProfilModel');
var MynModal=require('../models/MynModal');


  
module.exports.index=function(req,res){

    res.render('hasta');

}
module.exports.asiKaydet=function(req,res){


  console.log(req.body)
  res.render('hasta');

}

module.exports.hastaEkle=function(req,res){

   
      var sql = "SELECT * FROM IsDurumu WHERE AKTIF = 1;SELECT * FROM KanGrubu WHERE AKTIF = 1;SELECT * FROM MedeniHali WHERE AKTIF = 1;SELECT * FROM Meslekler;SELECT * FROM OgrenimDurumu WHERE AKTIF = 1;SELECT * FROM OzurlulukDurumu WHERE AKTIF = 1;SELECT * FROM SosyalGüvenceDurumu WHERE AKTIF = 1;SELECT * FROM Sehirler;SELECT * FROM Cinsiyet WHERE AKTIF=1;";
      sql+="SELECT * FROM YabanciHastaTuru WHERE AKTIF = 'true';SELECT * FROM UlkeKodlari WHERE AKTIF = 'true';";
      db.skrsConnection(sql,function(results){
        var params={
         IsDurumu:results[0],
         KanGrubu:results[1],
         MedeniHali:results[2],
         Meslekler:results[3],
         OgrenimDurumu:results[4],
         OzurlulukDurumu:results[5],
         SosyalGüvenceDurumu:results[6],
         İller:results[7],
         Cinsiyet:results[8],
         YabancıHastaTürü:results[9],
         UlkeKodlari:results[10],
        };

      res.render('hastaEklemeSayfası',params);

  });
   
}

module.exports.hastaEklePost=function(req,res){
 
  var form=req.body;
  

  hastaModel.hastaEkle(form,function(results){

      res.redirect("/");
  });
   




}
function getAge(DogumTarihi){

  var birthdate =new Date(DogumTarihi);
  var now = new Date();
  var dayDif=now.getDate()-birthdate.getDate();
  var yearDif=now.getFullYear()-birthdate.getFullYear();
  var monthDif=now.getMonth()-birthdate.getMonth();

  if(dayDif<0){monthDif--;dayDif+=30;if(monthDif<0){yearDif--;monthDif+=12;}}
  else{if(monthDif<0){yearDif--;monthDif+=12;}}
  var age="";
  if(yearDif>0){age+=yearDif+" Yıl ";}
  if(monthDif>0){age+=monthDif+" Ay ";}
  if(dayDif>0){age+=dayDif+" Gün ";}
  return age;

}
module.exports.hastaProfiliPost=function(req,res){
  var hasta=req.body.isim;
  var tc=hasta.split(" ")[0];

  hastaModel.hastaProfili(tc,function(MelissaResults,SkrsResults){
    MelissaResults=JSON.parse(JSON.stringify(MelissaResults));
    var hasta=MelissaResults[0][0];
    hasta.age=getAge(hasta.DogumTarihi);
    var HASTA_OZGEÇMİŞ=MelissaResults[2][0];
    //if(HASTA_OZGEÇMİŞ.DuzenliIlacKullanimi==undefined){HASTA_OZGEÇMİŞ.DuzenliIlacKullanimi='';}else{}

    hasta_ilacları=HASTA_OZGEÇMİŞ.DuzenliIlacKullanimi.split('?'); 
    hasta_ameliyat=HASTA_OZGEÇMİŞ.GecirdigiOperasyonlar.split('?'); 
    hasta_alerji=HASTA_OZGEÇMİŞ.Alerjileri.split('?'); 

    var params={
      hasta:hasta,
      hastanınKronikHastalıkları:MelissaResults[1],
      hasta_ilacları:hasta_ilacları,
      hasta_ameliyat:hasta_ameliyat,
      hasta_alerji:hasta_alerji,
      asilar:SkrsResults[0],
      asiuygulamasekli:SkrsResults[1],
      asikaynak:SkrsResults[2],
      asiuygulamayeri:SkrsResults[3],
      asidoz:SkrsResults[4],
      kronikHastalıklar:SkrsResults[5],
      ilaclar:SkrsResults[6],
    };
    req.session.hasta=hasta;
    res.render('hastaProfili',params);
    
  });

}
module.exports.hastaKronik=function(req,res){

  var secilenKronikler=req.query.secilenKronikler;
  var hastaId=req.session.hasta.Id;

  hastaProfilModel.hastaKronikGüncelle(hastaId,secilenKronikler,function(data){});

}
module.exports.hastailac=function(req,res){

  var secilenilaclar=req.query.secilenilaclar;
  var hastaId=req.session.hasta.Id;

  hastaProfilModel.hastailaclarıGüncelle(hastaId,secilenilaclar,function(data){});

}
module.exports.hastaAmeliyat=function(req,res){

  var secilenameller=req.query.secilenameller;
  var hastaId=req.session.hasta.Id;

  hastaProfilModel.hastaAmeliyatGüncelle(hastaId,secilenameller,function(data){});

}
module.exports.hastaAlerji=function(req,res){

  var secilenalerjiler=req.query.secilenalerjiler;
  var hastaId=req.session.hasta.Id;

  hastaProfilModel.hastaAlerjiGüncelle(hastaId,secilenalerjiler ,function(data){});

}

module.exports.kısaAnamnez=function(req,res){
  var hasta=req.session.hasta;

   
  hastaProfilModel.kısaMyn(function(results){
    
      var params={
          hasta:hasta,
          ICDLER:results[0],
          receturu:results[1],
          sevkNedeni:results[2],
          iller:results[3],
         
      };

      res.render('kısaAnamnez',params);

  });
   
  
}

module.exports.recete=function(req,res){
  var hasta=req.session.hasta;
  var tanılar=req.query.tanilar;

  if(tanılar==undefined){
    tanılar=req.session.tanilar;
  }
 
  hastaProfilModel.recete(function(results){

    var params={
        hasta:hasta,
        receteTuru:results[0],
        ilaclar:results[1],
        kullanımSekli:results[2],
        KullanimPeriyoduBirimi:results[3],
        SecilenTanılar:tanılar
    };


    res.render('kısaAnamnez-recete',params);

});
 

}

module.exports.kısaAnamnezBitir=function(req,res){
  
  res.render('hastaprofili');

}
module.exports.kısaAnamnezTanılarıKaydet=function(req,res){
  
  const form = JSON.parse(JSON.stringify(req.body))

  MynModal.KısaMynTanılarıKaydet(form,function(MelissaResults){          
  });
  res.redirect('/');

}

module.exports.rapor=function(req,res){


  var hasta=req.session.hasta;


   
  MynModal.rapor(function(results){

      var params={
          hasta:hasta,
          ICDLER:results,
         
      };


      res.render('hasta-Rapor',params);

  });
   
}
module.exports.istem=function(req,res){

  var hasta=req.session.hasta;


   
  MynModal.istem(function(results){


      var params={
          hasta:hasta,
          ICDLER:results[0],
          receturu:results[1],
          sevkNedeni:results[2],
          iller:results[3],
        
      };


      res.render('istem_sonuc',params);

  });
   

}
module.exports.detaylı=function(req,res){
  var hasta=req.session.hasta;
  var params={
    hasta:hasta
  };
  res.render('hasta-detaylı',params);

}

module.exports.detaylı2=function(req,res){

  var hasta=req.session.hasta;

  req.session.detaylıAnamnez1=req.body;
  var params={
    hasta:hasta
  };


  res.render('hasta-detaylı2',params);

}

module.exports.detaylı3=function(req,res){

  var hasta=req.session.hasta;

  req.session.detaylıAnamnez1=req.body;

  hastaModel.hastaProfili(hasta.Id,function(MelissaResults,SkrsResults){
    MelissaResults=JSON.parse(JSON.stringify(MelissaResults));
    var hasta=MelissaResults[0][0];
    hasta.age=getAge(hasta.DogumTarihi);
    var params={
      hasta:hasta,
      hastanınKronikHastalıkları:MelissaResults[1],
      kronikHastalıklar:SkrsResults[5],
    };
    req.session.hasta=hasta;
    res.render('hasta-detaylı3',params);
    
  });

 

}
module.exports.detaylı4=function(req,res){

  var hasta=req.session.hasta;

  req.session.detaylıAnamnez1=req.body;
  var params={
    hasta:hasta
  };


  res.render('hasta-detaylı4',params);

}

module.exports.detaylı5=function(req,res){

  var hasta=req.session.hasta;

  req.session.detaylıAnamnez1=req.body;
  var params={
    hasta:hasta
  };


  res.render('hasta-detaylı5',params);

}
module.exports.detaylı6=function(req,res){

  var hasta=req.session.hasta;

  req.session.detaylıAnamnez1=req.body;


   
  var sql = "SELECT * FROM ICD10 WHERE AKTIF = 1 LIMIT 100;"
  db.skrsConnection(sql,function(results){

      var params={
          hasta:hasta,
          ICDLER:results,
        
      };


      res.render('hasta-detaylı6',params);

  });


}

module.exports.detaylı7=function(req,res){

  var hasta=req.session.hasta;
  var tanılar=req.body.tanilar;
  req.session.tanilar=tanılar;
//  req.session.detaylıAnamnez1=req.body;


   
  MynModal.detaylı7(function(results){

      var params={
          hasta:hasta,
          ICDLER:results[0],
          SevkNedeni:results[1],
          Sehirler:results[2],

      };


      res.render('hasta-detaylı7',params);

  });


}

module.exports.fizikMyn=function(req,res){

  var hasta=req.session.hasta;



      var params={
          hasta:hasta,
        
      };


      res.render('fizikMyn',params);


}

