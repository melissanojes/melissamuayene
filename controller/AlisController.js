

var url="https://lab.ism.gov.tr/Alisws2konak/alisws.asmx";
var request = require('request');
var VENUSER= "EPHEAL"
var VENPASS="EPHEAL"
var parser = require('fast-xml-parser');
var DOKTOR_KODU='';

  
  module.exports.ALISDoktorBilgiSorgula=function(req,res){

  var xml='<?xml version="1.0" encoding="utf-8"?>';
  xml+='<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">';
  xml+='<soap:Body>';
  xml+='<ALISDoktorBilgiSorgula xmlns="http://tempuri.org/"><VENUSER>'+VENUSER+'</VENUSER><VENPASS>'+VENPASS+'</VENPASS><DOKTOR_KODU>'+DOKTOR_KODU+'</DOKTOR_KODU> </ALISDoktorBilgiSorgula>';
  xml+=' </soap:Body> </soap:Envelope>';  


  var hdr = {
    'content-type': 'text/xml; charset=utf-8',
    'SOAPAction': "http://tempuri.org/ALISDoktorBilgiSorgula",
  };

  var opt = {
      method: 'post',
      headers: hdr,
      body:xml,
      host:"lab.ism.gov.tr"
  };

    request(url,opt, function (error, response, body) {

        var str=body.split('<soap:Body>')[1];
        str=str.split('</soap:Body>')[0];

   });

}

module.exports.ALISBarkodBilgi=function(req,res){

    var xml='<?xml version="1.0" encoding="utf-8"?>';
    xml+='<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'
  xml+=' <soap:Body>'
  xml+='  <ALISBarkodBilgi xmlns="http://tempuri.org/">'
  xml+='   <VENUSER>'+VENUSER+'</VENUSER>'
  xml+='   <VENPASS>'+VENPASS+'</VENPASS>'
  xml+='     <ORNEKNO>'+ORNEKNO+'</ORNEKNO>'
  xml+=' </ALISBarkodBilgi>'
  xml+=' </soap:Body>'
  xml+='</soap:Envelope>'
  
  
    var hdr = {
      'content-type': 'text/xml; charset=utf-8',
      'SOAPAction': "http://tempuri.org/ALISBarkodBilgi",
    };
  
    var opt = {
        method: 'post',
        headers: hdr,
        body:xml,
        host:"lab.ism.gov.tr"
    };
  
  
      request(url,opt, function (error, response, body) {
  
          var str=body.split('<soap:Body>')[1];
          str=str.split('</soap:Body>')[0];
  
     });
  
  }

  module.exports.ALISDiyalizSonucDurum=function(req,res){

    var ORNEKNO='999999991';
    var BASTAR,BITTAR,TESTLER,TC_KIMLIK_NO;
     var xml='<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">';
     xml+='<soap:Body><ALISDiyalizSonucDurum xmlns="http://tempuri.org/"><VENUSER>'+VENUSER+'</VENUSER><VENPASS>'+VENPASS+'</VENPASS>';
     xml+='<ORNEKNO>'+ORNEKNO+'</ORNEKNO><BASTAR>'+BASTAR+'</BASTAR><BITTAR>'+BITTAR+'</BITTAR><TESTLER>'+TESTLER+'</TESTLER><TC_KIMLIK_NO>'+TC_KIMLIK_NO+'</TC_KIMLIK_NO>';
     xml+='</ALISDiyalizSonucDurum></soap:Body></soap:Envelope>';
  
  
    var hdr = {
      'content-type': 'text/xml; charset=utf-8',
      'SOAPAction': "http://tempuri.org/ALISDiyalizSonucDurum",
    };
  
    var opt = {
        method: 'post',
        headers: hdr,
        body:xml,
        host:"lab.ism.gov.tr"
    };
  
  
      request(url,opt, function (error, response, body) {
  
          var str=body.split('<soap:Body>')[1];
          str=str.split('</soap:Body>')[0];
  
     });
  
  }
  module.exports.ALISEntegKodlari=function(req,res){

     var xml='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/"> <soapenv:Header/>';
     xml+='<soapenv:Body><tem:ALISEntegKodlari><!--Optional:--><tem:VENUSER>'+VENUSER+'</tem:VENUSER><!--Optional:--><tem:VENPASS>'+VENPASS+'</tem:VENPASS>';
     xml+='</tem:ALISEntegKodlari></soapenv:Body></soapenv:Envelope>';
  
  
    var hdr = {
      'content-type': 'text/xml;charset=UTF-8',
      'SOAPAction': "http://tempuri.org/ALISEntegKodlari",
    };
  
    var opt = {
        method: 'post',
        headers: hdr,
        body:xml,
        host:"lab.ism.gov.tr",
        Connection:'Keep-Alive'
    };
    var parsingOptions = {
      
    };
    request(url,opt, function (error, response, body) {
       
    var jsonResult = parser.parse(body, parsingOptions); //xml to json
    var entegreler=jsonResult['soap:Envelope']['soap:Body']['ALISEntegKodlariResponse']['ALISEntegKodlariResult']['EntegKodArr']['EntegKodlar'];
     res.json({entegreler:entegreler});
  
     });
  
  }
  