






module.exports.SYSTakipNoSorgulama=function(){


    var fs =require('fs');
var request = require('request');
var parser = require('fast-xml-parser');

    const xml = fs.readFileSync('./soapOperations/SYSTakipNoSorgulama.xml', 'utf-8'); 
    const url= 'https://systest.sagliknet.saglik.gov.tr/SYS/SYSWS.svc?wsdl';



    var requestHeaders = {
        'cache-control': 'no-cache',
        'content-type': 'text/xml;charset=UTF-8',
        'Content-Length':xml.length,
        'SOAPAction':"https://sys.sagliknet.saglik.gov.tr/SYS/ISYSWS/SYSSendMessage"
    
     };
     var requestOptions = {
         method: 'POST',
         url: url,
         headers: requestHeaders,
         body: xml,
          rejectUnauthorized: false,   
     };
  
    var options = {ignoreComment: true, alwaysChildren: true};

    request(requestOptions, function (error, response, body) {
            if (error) {
                 // handle error
             } else {    
            try {
                 var parsingOptions = {
               
            };

            console.log(body);

            var jsonResult = parser.parse(body, parsingOptions); //xml to json




            var newXml=jsonResult['s:Envelope']['s:Body']['SYSSendMessageResponse']['SYSSendMessageResult'];// parsing object

            var jsonResult2 = parser.parse(newXml, parsingOptions); // from xml
            var recordData=jsonResult2['SYSMessage']['recordData'];
            var KayitCevabi=jsonResult2['SYSMessage']['recordData']['KayitCevabi'];
            var hastaSaglikKayitlari=jsonResult2['SYSMessage']['recordData']['KayitCevabi']['HASTA_SAGLIK_KAYITLARI']['recordData'];
            var hastaKimlikBilgileri=jsonResult2['SYSMessage']['recordData']['KayitCevabi']['HASTA_SAGLIK_KAYITLARI']['recordData']['HASTA_KIMLIK_BILGILERI'];
            var hastaBasvuruBilgileri=jsonResult2['SYSMessage']['recordData']['KayitCevabi']['HASTA_SAGLIK_KAYITLARI']['recordData']['HASTA_BASVURU_BILGILERI'];

            //console.log(hastaBasvuruBilgileri);
                return KayitCevabi;
             } catch (e) {
                  // handle error
             console.log(e);
    }
   }
});


};

