var express=require('express');

var router=express.Router();

var ctrlHome=require('../controller/HomeController');

router.get('/',ctrlHome.index);
router.get('/showPDF',ctrlHome.showPDF);


router.get('/ilceleriGetir',ctrlHome.ilKoduylaİlçeleriGetir);
router.get('/koyleriGetir',ctrlHome.ilceKoduylaKöyleriGetir);
router.get('/mahalleGetir',ctrlHome.köyKoduylaMahalleGetir);

router.post('/mernisTcSorgula',ctrlHome.mernisTcSorgula);
router.post('/hastaFilter',ctrlHome.hastaFilter);
router.post('/guncelleHome',ctrlHome.guncelleHome);

router.get('/tanFilter',ctrlHome.tanilarıFiltrele);
router.get('/kronikFilter',ctrlHome.kronikleriFiltrele);

router.get('/ilacFilter',ctrlHome.ilaclarıFiltrele);


router.get('/imzatest',ctrlHome.imza);
router.get('/test',ctrlHome.test);
router.post('/testpost',ctrlHome.testpost);

router.get('/testnodejsimzaIndex',ctrlHome.testimzaIndex);
router.get('/Home/GetStatus',ctrlHome.downloadSignedFile);
router.post('/renkliRecete',ctrlHome.renkliRecete);







router.get('/testandroidimza',ctrlHome.testimzaAndroid);



module.exports=router;