var express=require('express');

var router=express.Router();

var ctrlAyarlar=require('../controller/AyarlarController');

router.get('/',ctrlAyarlar.index);

router.get('/malzemeIslemleri',ctrlAyarlar.malzemeIslemleri);

router.get('/malzemeIslemleri/stokKartlari',ctrlAyarlar.stokKartları);
router.get('/malzemeIslemleri/stokHareketleri',ctrlAyarlar.stokHareketleri);
router.get('/malzemeIslemleri/eskiYeniKullaniciAnalizi',ctrlAyarlar.kullaniciAnalizi);

router.get('/malzemeIslemleri/miadAnalizi',ctrlAyarlar.miad);


module.exports=router;