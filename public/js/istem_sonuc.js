$(document).ready(function(){
 
 


    $( "#temizleSevketModal").click(function() {
    
       
        $("#sevketModal")
        .find("input,textarea,select")
        .val('')
        .end()
        .find("input[type=checkbox], input[type=radio]")
        .prop("checked", "")
        .end();
    
    
    });
    
    var secilenTanılar=[];
    
    $( "#receteBtn").click(function() {
        $('#kisaAnamnezForm').attr('action', '/hasta/kisaAnamnez/recete');
        $('#kisaAnamnezForm').attr('method', 'get');
        secilenTanılar.forEach(element => {
            var string=element.kod+"?"+element.taniTuru+"?"+element.isim;
            var input = $("<input>").attr("type", "hidden").attr("name", "tanilar").val(string);
            $('#kisaAnamnezForm').append(input);
        });
    
        $('#kisaAnamnezForm').submit();
    });
    
    $( "#mynBitirBtn").click(function() {
        $('#mynBitirForm').submit();
    });
    
    
    
    $(document).on('click', ".taniRow", function(){
        //Your code
    
        if($(this).children(".cbx").children().css('background-color')=="rgb(255, 0, 0)"){//kırmızı mı
            $(this).children(".cbx").children().css('background-color','rgb(0, 255, 0)');//yeşil
            var kod= $(this).children(".cbx").children().attr("name").split("?")[0];;
            var isim= $(this).children(".cbx").children().attr("name").split("?")[1];;
    
           var html= '<div  class="row secilenTanilarTable ">';
           html+= '<div class="col-md-4 kod" style="background-color: white;text-align: center;padding: 10px;border-bottom: solid;">'+kod+'</div>'; 
           html+= '<div class="col-md-4 tür" style="background-color: white;text-align: center;padding: 10px;border-bottom: solid;">'+tanıTürü+'</div>';  
           html+= '<div class="col-md-4 isim" style="background-color: white;padding: 10px;text-align: center;border-bottom: solid;">'+isim+'</div> </div>';
          
            $("#secilenTanilarTable").append(html);
            var t={kod:kod,taniTuru:tanıTürü,isim:isim};
            secilenTanılar.push(t);
        }
        else{
            $(this).children(".cbx").children().css('background-color','red')
            var kod= $(this).children(".cbx").children().attr("name").split("?")[0];
            var str=":contains("+kod+")" ;
            $(".secilenTanilarTable").remove(str);
            secilenTanılar.forEach(element => {
                if(element.kod==kod){
                    secilenTanılar = jQuery.grep(secilenTanılar, function(value) {
                        return value != element;
                      });
    
                }
            });
    
        }
    });
        $("#taniAra").on("keyup", function () {
            var value = $(this).val().toLowerCase();

                $( ".taniRow" ).remove();
                $.get( "/tanFilter", { value:value} )
                    .done(function( tanılar ) {
                      
                                $.each(tanılar, function (index, value) {
                                    color="red"
                                    var found_names = $.grep(secilenTanılar, function(v) {
                                        return v.kod === value.KODU;
                                    });
                                    if (found_names.length==1){color="green"}
                                  var html= '<div class="row taniRow">';
                                  html+= '<div class="col-md-2 cbx" style="background-color: white;text-align: right;padding: 10px;border-bottom: solid;">'; 
                                  html+= '<div class="tik" style="background-color:'+color+';height: 25px;width: 25px;margin-left: 46%;" name="'+value.KODU+'?'+value.ADI+'"></div></div> ';  
                                  html+= ' <div class="col-md-4 kod" style="background-color: white;text-align: center;padding: 10px;border-right: solid;border-bottom: solid;">'+value.KODU+'</div>  ';
                                  html+= '<div class="col-md-6 isim" style="background-color: white;padding: 10px;text-align: center;border-bottom: solid;">'+value.ADI+'</div> </div>';
          
                                  $( ".headerDiv" ).after(html);         
                                });  
                });
    
        });
    
    
    
    var tanıTürü="Ön Tanı";
    $( "#anaTanı").click(function() {
        if($(this).hasClass("selected")){
        }
        else{
            $(".tanıTür").css("background-color","#b5d8e5");
            $(".tanıTür").removeClass("selected");
            $(this).css("background-color","white");
            $(this).addClass("selected");
            tanıTürü=$(this).text();
        }
    });
    $( "#ekTanı").click(function() {
        if($(this).hasClass("selected")){
           
        }
        else{
            $(".tanıTür").css("background-color","#b5d8e5");
            $(".tanıTür").removeClass("selected");
            $(this).css("background-color","white");
            $(this).addClass("selected");
            tanıTürü=$(this).text();
    
        }
    });
    $( "#ayırıcıTanı").click(function() {
        if($(this).hasClass("selected")){
        
        }
        else{
            $(".tanıTür").css("background-color","#b5d8e5");
            $(".tanıTür").removeClass("selected");
            $(this).css("background-color","white");
            $(this).addClass("selected");
            tanıTürü=$(this).text();
    
        }
    });
    $( "#onTanı").click(function() {
        if($(this).hasClass("selected")){
          
        }
        else{
            $(".tanıTür").css("background-color","#b5d8e5");
            $(".tanıTür").removeClass("selected");
            $(this).css("background-color","white");
            $(this).addClass("selected");
            tanıTürü=$(this).text();
    
        }
    });
    
    
    
    
    
    $("#datepicker").change(function() {
        if($(this).prop('checked')) {
            $(this).datepicker();
    
        } else {
            $(this).datepicker("hide");
        }
    });
    
    
    
    
    //_-----------------il seçince ilçeler gelsin
    
    
    $('#iller').on('change', function() {
        var ilkodu= this.value;
    
        $.ajax({
            url: '/ilceleriGetir',
            method: 'get',
            data: { ilkodu: ilkodu }
            }).done(function(res) {
                var ilceArray=res.ilceler;
                for (var i = 0; i <= ilceArray.length; i++) {
                    $('#ilceler').append('<option value="' + ilceArray[i].KODU + '">' + ilceArray[i].ADI + '</option>');
                }
        });
     
    });
    
    });

    var entegreler;
    var secilenTestler=[];


        $.ajax({
            url: '/Alis/entegKodlari',
            method: 'get',
            }).done(function(res) {
                entegreler=res.entegreler;
                var tetkikTürleri=Array();
                for (var i = 0; i <entegreler.length; i++) {
                    tetkikTürleri.push(entegreler[i].UNITE_ADI);
                }
                tetkikTürleri=unique(tetkikTürleri);


                for (var i = 0; i < tetkikTürleri.length; i++) {
                    if(tetkikTürleri[i].UNITE_ADI=="Hemogram"){
                         $('#tetkikTürleri').append('<option selected value="' + tetkikTürleri[i] + '">' + tetkikTürleri[i] + '</option>');
                    }
                    else{
                        $('#tetkikTürleri').append('<option value="' + tetkikTürleri[i] + '">' + tetkikTürleri[i] + '</option>');
                    }
                }

                $.each(entegreler, function (index, value) {
                    if(value.UNITE_ADI==$("#tetkikTürleri").val()){
                                color="red"
                                // var found_names = $.grep(secilenTanılar, function(v) {
                                //     return v.kod === value.KODU;
                                // });
                                // if (found_names.length==1){color="green"}
                              var html= '<div class="row testRow">';
                              html+= '<div class="col-md-3 cbx" style="background-color: white;text-align: right;padding: 10px;border-bottom: solid;">'; 
                              html+= '<div class="tik" style="background-color:'+color+';height: 25px;width: 25px;margin-left: 46%;" name="'+value.ENTEG_KODU+'?'+value.TEST_KISA_ADI+'"></div></div> ';  
                              html+= '<div class="col-md-9 isim" style="background-color: white;padding: 10px;text-align: center;border-bottom: solid;">'+value.TEST_KISA_ADI+'</div> </div>';
      
                              $( ".testlerheaderDiv" ).after(html);        
                    } 
                 });  
        
        });
       

    function unique(list) {
        var result = [];
        $.each(list, function(i, e) {
          if ($.inArray(e, result) == -1) result.push(e);
        });
        return result;
      }
      

      $("#tetkikTürleri").on("change", function () {

            $( ".testRow" ).remove();

            $.each(entegreler, function (index, e) {
                    if(e.UNITE_ADI==$("#tetkikTürleri").val()){
                        console.log(secilenTestler);
                                color="red"
                                 var found_names = $.grep(secilenTestler, function(v) {
                                     return v.entegKodu == e.ENTEG_KODU;
                                 });
                                 if (found_names.length!=0){color="green"}
                              var html= '<div class="row testRow">';
                              html+= '<div class="col-md-3 cbx" style="background-color: white;text-align: right;padding: 10px;border-bottom: solid;">'; 
                              html+= '<div class="tik" style="background-color:'+color+';height: 25px;width: 25px;margin-left: 46%;" name="'+e.ENTEG_KODU+'?'+e.TEST_KISA_ADI+'"></div></div> ';  
                              html+= '<div class="col-md-9 isim" style="background-color: white;padding: 10px;text-align: center;border-bottom: solid;">'+e.TEST_KISA_ADI+'</div> </div>';
      
                              $( ".testlerheaderDiv" ).after(html);        
                    } 
            });  
    });


$(document).on('click', ".testRow", function(){
    //Your code

    if($(this).children(".cbx").children().css('background-color')=="rgb(255, 0, 0)"){//kırmızı mı
        $(this).children(".cbx").children().css('background-color','rgb(0, 255, 0)');//yeşil
        var entegKodu= $(this).children(".cbx").children().attr("name").split("?")[0];;
        var testKısaAdı= $(this).children(".cbx").children().attr("name").split("?")[1];;

       var html= '<div  class="row secilenTestlerTable ">';
       html+= '<div class="col-md-1 entegKodu" style="background-color: white;text-align: center;padding: 10px;border-bottom: solid;display:none;">'+entegKodu+'</div>'; 
       html+= '<div class="col-md-12 testAdı" style="background-color: white;padding: 10px;text-align: center;border-bottom: solid;border-width: thin;">'+testKısaAdı+'</div> </div>';
      
        $("#secilenTestlerTable").append(html);
        var t={entegKodu:entegKodu,testKısaAdı:testKısaAdı};

        secilenTestler.push(t);
    }
    else{
        $(this).children(".cbx").children().css('background-color','red')
        var entegKodu= $(this).children(".cbx").children().attr("name").split("?")[0];
        var str=":contains("+entegKodu+")" ;
        $(".secilenTestlerTable").remove(str);
        secilenTestler.forEach(element => {
            if(element.entegKodu==entegKodu){
                secilenTestler = jQuery.grep(secilenTestler, function(value) {
                    return value != element;
                  });

            }
        });

    }
});